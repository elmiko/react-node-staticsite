all: dist public bundle.js 

.PHONY: dist public jsx clean
.IGNORE: dist public clean

dist:
	mkdir dist 2>/dev/null

public:
	cp -r public/* dist/

jsx:
	./node_modules/.bin/babel src --out-dir build --presets @babel/preset-react
	cp src/*.css build/ 2> /dev/null

bundle.js: jsx
	./node_modules/.bin/webpack

clean:
	rm -r build dist 2> /dev/null

