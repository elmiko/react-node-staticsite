# react-node-staticsite

this project is a skeleton that helps with rapid early development of static
react-based websites. what this means is that this project will bundle react
and the associated user code within into a set of files that can be served
by any http server as a static website.

there is also some helper functionality for deploying on
[openshift](https://okd.io) through the
[source-to-image](https://github.com/openshift/source-to-image) project.
